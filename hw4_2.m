disp('Problem 2')

image1 = imread('3.bmp');
image1 = rgb2gray(image1);
image1 = imgaussfilt(image1,3);
%imshow(image1)

s = size(image1);
kernel_size = 81;
diff = floor(kernel_size/2);

% Convolve the image
image1_mod = image1;
%sigmas = [(0.1)*kernel_size/3:(0.3)*kernel_size/3:kernel_size/3];
sigmas = [1:6:25];
responses = zeros([length(sigmas) s(1) s(2)]);
for t=sigmas
    disp('Scale = '); disp(t);
    kernel = t^2*fspecial('log', [t*3 t*3], t);
    kernel_size = t*3;
    diff = floor(t*3/2);
    % Normalize kernel such that sum of elements is 1
%     kernel = kernel/sum(sum(abs(kernel)))
    % Iterate over RGB channels
    %for i=1:1:s(3)
        % Iterate over rows and columns
        for y=1+diff:1:s(1)-diff
            for x=1+diff:1:s(2)-diff
                % Pick region of interest
                roi = image1(y-diff:y+diff, ...
                    x-diff:x+diff ...
                    );
                % Apply convolution operation 
                % to selected region
                response = mean(sum( ...
                    rot90(rot90(kernel))*double(roi) ...
                    ));
                responses(find(sigmas==t,1), y, x) = response^2;
            end
        end
    %end
end
disp('Done scales')
%%
disp('Generating blobs')
[responses_max, responses_indices] = max(responses);
blobs = [];
threshold = 350;
for i=1:length(sigmas)
    reponses_max(i,:,:) = ordfilt2(reshape(responses_max(i,:,:), [s(1) s(2)]), ...
        9, ones(3,3));
end
for x=1+t:s(2)-t
    for y=1+t:s(1)-t
        radius = sqrt(2)*responses_indices(1, y, x)^2;
%         roi = responses_max(1, ...
%             y-floor(radius/2):y+floor(radius/2), ...
%             x-floor(radius/2):x+floor(radius/2));
%         if responses_max(1, y, x) > threshold && ...
%                 responses_max(1, y, x) >= max(max(roi))
%             blobs = [blobs; x, y, radius];
%         end

        if responses_max(1, y, x) > threshold
            blobs = [blobs; x, y, radius];
        end
    end
end
show_all_circles(image1, blobs(:,1), blobs(:,2), blobs(:,3))
disp('Done thresholding')