image1_color = imread('I1.png');
%image2_color = imread('I3.png');
image1 = rgb2gray(image1_color);
%image2 = rgb2gray(image2_color);
%image1 = imgaussfilt(image1,1);
%image2 = imgaussfilt(image2,1);
%imshow(image1)
%imshow(image2)

s = size(image1);
%kernel_size = 81;
%diff = floor(kernel_size/2);

% Convolve the image
%image1_mod = image1;
dx = [
    -1 0 1; 
    -1 0 1; 
    -1 0 1];
dy = dx';

responses = zeros([2 s(1) s(2)]);

%sigma = 3;
kernel_size = 3;
%kernel = fspecial('gaussian', [kernel_size kernel_size], sigma);
diff = floor(kernel_size/2);
% Normalize kernel such that sum of elements is 1
%     kernel = kernel/sum(sum(abs(kernel)))
% Iterate over rows and columns
for y=1+diff:1:s(1)-diff
    for x=1+diff:1:s(2)-diff
        % Pick region of interest
        roi = image1(y-diff:y+diff, ...
            x-diff:x+diff ...
            );
        % Apply convolution operation 
        % to selected region
        % dx - verified
        response = mean(sum( ...
            double(roi)*rot90(rot90(dy)) ...
            ));
        responses(2, y, x) = response;
        % dy - verified
        response2 = mean(sum( ...
            rot90(rot90(dx))*double(roi) ...
            ));
        responses(1, y, x) = response2;
    end
end
disp('Done')
%%

%imshow(conv2(image1, dx, 'same'))
%figure, imshow(reshape(responses(2,:,:), s(1), s(2)))
%figure; imshow(image1)
diff = 2;
corners = [];
kernel = diag(reshape(fspecial('gaussian', [(diff*2+1) (diff*2+1)], diff), ...
    (diff*2+1)*(diff*2+1), 1 ...
    ));

for y=1+diff:1:s(1)-diff
    for x=1+diff:1:s(2)-diff
%for y=1+diff+180:1:220%s(1)-diff-3*s(1)/5
%    for x=1+diff+60:1:120%s(2)-diff-3*s(2)/4
    % Pick region of interest
        roi_x = reshape(...
                        responses(1, y-diff:y+diff, ...
                        x-diff:x+diff ...
                        ), ...
                        (diff*2+1)*(diff*2+1), 1 ...
                        );
        roi_y = reshape(...
                        responses(2, y-diff:y+diff, ...
                        x-diff:x+diff ...
                        ), ...
                        (diff*2+1)*(diff*2+1), 1 ...
                        );
            
        M = [ roi_x'*kernel*roi_x roi_x'*kernel*roi_y;
              roi_x'*kernel*roi_y roi_y'*kernel*roi_y;
            ];
        
        %l = eig(M);
        [U,S,V] = svd(M);
        l = [S(2,2) S(1,1)];
        %if l(1) > l(2)
        angle = atan2(U(2,1), U(1,1));
            %disp('1')
        %else
        %    angle = atan2(U(2,2), U(1,2));
            %disp('2')
        %end
        %l = l/norm(l);
        k = 0.04;
        cr = l(1)*l(2) - k*(l(1) + l(2))^2;
        %if (1 || cr > 1e9)
        if (cr > 1.9e8)
            % corner
            corners = [ corners; ...
                x, y, 1, 1e2*1/sqrt(l(1)), 1e2*1/sqrt(l(2)), ...
                angle];
        end
        %if (cr < -1e11)
            % corner
        %    corners = [ corners; x, y, 1, 1e3*1/sqrt(l(1)), 1e3*1/sqrt(l(2)), pi];
        %end
    end
end
disp('Corners Computed')
%%
%show_all_circles(image1, corners(:,1), corners(:,2), corners(:,3))
imshow(image1)  
hold on 
for i=1:length(corners(:,4)) 
    plot_ellipse(corners(i,4), corners(i,5), ...
        corners(i,1), corners(i,2), ...
        corners(i,6), 'r')
end
disp('Ellipses Drawn')